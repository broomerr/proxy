package net.extracode.mule.beans.proxy.test;

import net.extracode.mule.beans.proxy.AccentureTokenGetter;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;

public class AccentureTokenGetterTest {

    @Test
    @Ignore
    public void oauth() throws IOException {
        AccentureTokenGetter accentureTokenGetter = new AccentureTokenGetter();
        accentureTokenGetter.setUrl("https://federation-sts-stage.accenture.com/services/jwt/issue/adfs");
        accentureTokenGetter.setGrantType("password");
        accentureTokenGetter.setUsername("A04213_EZWim");
        accentureTokenGetter.setPassword("vL32iI1Ek74Kp950gY66");
        accentureTokenGetter.setScope("MRDR:WebService:testing");
        System.out.println(accentureTokenGetter.getToken());
    }

    @Test
    @Ignore
    public void oauth2() throws IOException {
        AccentureTokenGetter accentureTokenGetter = new AccentureTokenGetter();
        accentureTokenGetter.setUrl("https://federation-sts-stage.accenture.com/oauth/ls/connect/token");
        accentureTokenGetter.setGrantType("Client Credentials");
        accentureTokenGetter.setUsername("4213.ezwim.service");
        accentureTokenGetter.setPassword("jhktq4tog6a");
        accentureTokenGetter.setScope("read_chargecode_mrdr_chargecode execute_mrdr_chargecode");
        System.out.println(accentureTokenGetter.getOauth2Token());
    }
}
