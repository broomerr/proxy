package net.extracode.mule.beans.proxy.test;

import net.extracode.mule.beans.proxy.AccentureServiceCall;
import net.extracode.mule.beans.proxy.AccentureTokenGetter;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;

public class AccentureServiceCallTest {

    private AccentureServiceCall accentureServiceCall = new AccentureServiceCall();

    @Before
    public void init() {
        accentureServiceCall.setChargeCodes("A3NSL00399");
    }

    @Test
    @Ignore
    public void oauth() throws IOException {

        AccentureTokenGetter accentureTokenGetter = new AccentureTokenGetter();
        accentureTokenGetter.setUrl("https://federation-sts-stage.accenture.com/services/jwt/issue/adfs");
        accentureTokenGetter.setGrantType("password");
        accentureTokenGetter.setUsername("A04213_EZWim");
        accentureTokenGetter.setPassword("vL32iI1Ek74Kp950gY66");
        accentureTokenGetter.setScope("MRDR:WebService:testing");

        accentureServiceCall.setServiceUrl("https://mrdr-chargecode.cioprodfix.accenture.com/1033_MRDR_ChargeCode_16_3_0/OAuth1/ChargeCode-Service/ChargeCode/GetChargeCodeDetail");
        accentureServiceCall.setToken(accentureTokenGetter.getToken());
        System.out.println(accentureServiceCall.getChargeCodeDetails());
    }

    @Test
    @Ignore
    public void oauth2() throws IOException {

        AccentureTokenGetter accentureTokenGetter = new AccentureTokenGetter();
        accentureTokenGetter.setUrl("https://federation-sts-stage.accenture.com/oauth/ls/connect/token");
        accentureTokenGetter.setGrantType("Client Credentials");
        accentureTokenGetter.setUsername("4213.ezwim.service");
        accentureTokenGetter.setPassword("jhktq4tog6a");
        accentureTokenGetter.setScope("read_chargecode_mrdr_chargecode execute_mrdr_chargecode");

        accentureServiceCall.setServiceUrl("https://mrdr-aws-rel1.ciostage.accenture.com/rel1/chargecode-service/chargecode/getchargecodedetail");
        accentureServiceCall.setToken(accentureTokenGetter.getOauth2Token());
        accentureServiceCall.setApiKey("9imaoeldt13pOhIsCaY89GT3UIJj9PjwKIRELY60");
        System.out.println(accentureServiceCall.getChargeCodeDetailsOAuth2());
    }
}
