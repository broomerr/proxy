package net.extracode.mule.beans.proxy.test;

import net.extracode.mule.beans.proxy.PropertiesToStringInjector;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.Properties;

public class PropertiesToStringInjectorTest {

    private static final String INJECT_TEST_STRING_PATH = "/inject_test.xml";
    private static final String INJECT_TEST_RESULT_PATH = "/inject_test_result.xml";
    private static final String INJECT_TEST_PROPS_PATH = "/inject_test.properties";
    private static final String INJECT_TEST_PATTERN = "#(.*?)#";

    @Test
    public void testInject() throws IOException {
        String string = IOUtils.toString(
                PropertiesToStringInjectorTest.class.getResourceAsStream(INJECT_TEST_STRING_PATH));
        Properties properties = new Properties();
        properties.load(PropertiesToStringInjectorTest.class.getResourceAsStream(INJECT_TEST_PROPS_PATH));
        String result = new PropertiesToStringInjector().inject(string, properties, INJECT_TEST_PATTERN);
        String testResult = IOUtils.toString(
                PropertiesToStringInjectorTest.class.getResourceAsStream(INJECT_TEST_RESULT_PATH));
        Assert.assertTrue(result.equals(testResult));
    }

}
