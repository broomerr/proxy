package net.extracode.mule.beans.proxy;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mule.api.MuleEventContext;
import org.mule.api.lifecycle.Callable;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PropertiesToStringInjector implements Callable {

    private static final Logger logger = LogManager.getLogger(PropertiesToStringInjector.class);

    private static final String DEFAULT_PATTERN = "#(.*?)#";

    private String targetStringVar;
    private String propertiesPathVar;
    private String resultVar;
    private String patternVar;

    @Override
    public Object onCall(MuleEventContext muleEventContext) throws Exception {
        logger.info("---ENTRY---");
        String targetString = muleEventContext.getMessage().getInvocationProperty(getTargetStringVar());
        if (targetString == null) {
            throw new NullPointerException("targetString is null");
        }
        logger.debug("targetString:\n" + targetString);
        String propertiesPath = muleEventContext.getMessage().getInvocationProperty(getPropertiesPathVar());
        if (propertiesPath == null) {
            throw new NullPointerException("propertiesPath is null");
        }
        logger.debug("propertiesPath:\n" + propertiesPath);
        if (!new File(propertiesPath).exists()) throw new IOException("properties file not exists");
        Properties properties = new Properties();
        properties.load(new FileInputStream(propertiesPath));

        String pattern = muleEventContext.getMessage().getInvocationProperty(getPatternVar(), DEFAULT_PATTERN);
        logger.debug("pattern:\n" + pattern);

        muleEventContext.getMessage().setInvocationProperty(getResultVar(), inject(targetString, properties, pattern));
        logger.info("---END---");
        return muleEventContext.getMessage().getPayload();
    }

    public String inject(String string, Properties properties, String pattern) {
        Pattern template = Pattern.compile(pattern);
        Matcher m = template.matcher(string);
        while (m.find()) {
            String property = properties.getProperty(m.group(1), "");

            if (property.isEmpty()) {
                logger.warn("Property for key '" + m.group(1) + "' not found or set!");
            }

            string = m.replaceFirst(property);
            m.reset(string);
        }
        return string;
    }

    public String getPropertiesPathVar() {
        return propertiesPathVar;
    }

    public void setPropertiesPathVar(String propertiesPathVar) {
        this.propertiesPathVar = propertiesPathVar;
    }

    public String getTargetStringVar() {
        return targetStringVar;
    }

    public void setTargetStringVar(String targetStringVar) {
        this.targetStringVar = targetStringVar;
    }

    public String getResultVar() {
        return resultVar;
    }

    public void setResultVar(String resultVar) {
        this.resultVar = resultVar;
    }

    public String getPatternVar() {
        return patternVar;
    }

    public void setPatternVar(String patternVar) {
        this.patternVar = patternVar;
    }
}
