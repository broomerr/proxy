package net.extracode.mule.beans.proxy;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.mule.api.MuleEventContext;
import org.mule.api.lifecycle.Callable;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class AccentureServiceCall implements Callable {

    private static final Logger logger = LogManager.getLogger(AccentureServiceCall.class);

    private static final String CONTENT_TYPE_HEADER = "Content-Type";
    private static final String CONTENT_TYPE = "application/json";
    private static final String AUTHORIZATION_HEADER = "Authorization";
    private static final String AUTHORIZATION_TOKEN_HEADER = "authorizationToken";
    private static final String AUTHORIZATION_PREFIX = "Bearer ";
    private static final String API_KEY_HEADER = "x-api-key";
    private static final String CHARGE_CODES_JSON_FIELD_NAME = "ChargeCodes";
    private static final String MFMP_INVALID_JSON_FIELD_NAME = "MarkFedMPInvalid";
    private static final String MFMP_INVALID_JSON_FIELD_VALUE = "false";
    private static final String PERS_NUM_JSON_FIELD_NAME = "PersonnelNumber";
    private static final String PERS_NUM_JSON_FIELD_VALUE = "";
    private static final String REQUEST_METHOD = "POST";
    private static final String ENCODING = "UTF-8";
    private static final int JSON_INDENTATION = 2;
    private static final String JSON_CONTEXT_FIELD_TO_REMOVE = "@odata.context";
    private static final String JSON_ROOT_FIELD_TO_ADD = "ArrayOfChargeCodeDetail";
    private static final String JSON_FIELD_TO_RENAME = "value";
    private static final String JSON_FIELD_NEW_NAME = "ChargeCodeDetail";

    private static final String OAUTH1_TOKEN_TYPE = "oauth1";
    private static final String OAUTH2_TOKEN_TYPE = "oauth2";

    private String serviceUrlVar;
    private String chargeCodesVar;
    private String tokenVar;
    private String apiKeyVar;
    private String responseVar;
    private String tokenTypeVar;

    private String serviceUrl;
    private String chargeCodes;
    private String token;
    private String apiKey;
    private String tokenType;

    @Override
    public Object onCall(MuleEventContext muleEventContext) throws Exception {
        logger.info("execution");

        serviceUrl = muleEventContext.getMessage().getInvocationProperty(getServiceUrlVar());
        if (serviceUrl == null) throw new NullPointerException("service url is not defined");
        logger.debug("service url: " + serviceUrl);
        chargeCodes = muleEventContext.getMessage().getInvocationProperty(getChargeCodesVar());
        if (chargeCodes == null) throw new NullPointerException("charge codes is not defined");
        logger.debug("charge codes: " + chargeCodes);
        token = muleEventContext.getMessage().getInvocationProperty(getTokenVar());
        if (token == null) throw new NullPointerException("token getter is not defined");
        logger.debug("token: " + token);
        tokenType = muleEventContext.getMessage().getInvocationProperty(getTokenTypeVar());
        if (tokenType == null) throw new NullPointerException("token type is not defined");
        logger.debug("tokenType: " + tokenType);

        if (tokenType.equals(OAUTH1_TOKEN_TYPE)) {
            logger.info("oauth 1 token type is chosen");
            muleEventContext.getMessage().setInvocationProperty(getResponseVar(), getChargeCodeDetails());
        }
        else if (tokenType.equals(OAUTH2_TOKEN_TYPE)) {
            logger.info("oauth 2 token type is chosen");
            apiKey = muleEventContext.getMessage().getInvocationProperty(getApiKeyVar(), "");
            logger.debug("api key: " + apiKey);
            muleEventContext.getMessage().setInvocationProperty(getResponseVar(), getChargeCodeDetailsOAuth2());
        }

        logger.info("complete");
        return muleEventContext.getMessage();
    }

    public String getChargeCodeDetails(HttpURLConnection connection) throws IOException {

        connection.setDoOutput(true);
        connection.setDoInput(true);
        try (DataOutputStream dataOutputStream = new DataOutputStream(connection.getOutputStream())) {
            logger.debug("json body:\n" + getBodyJson(getChargeCodes()));
            dataOutputStream.write(getBodyJson(getChargeCodes()).getBytes(ENCODING));
        }
        int responseCode = connection.getResponseCode();
        logger.debug("responseCode: " + responseCode);
        if (responseCode != 200) {
            logger.error("not successful response code: " + responseCode);
            throw new RuntimeException("not successful response code: " + responseCode);
        }
        String response = IOUtils.toString(connection.getInputStream(), ENCODING);
        logger.debug("response:\n" + response);
        logger.debug("post processed response:\n" + responsePostProcessing(response));
        return responsePostProcessing(response);
    }

    public String getChargeCodeDetailsOAuth2() throws IOException {
        URL url = new URL(getServiceUrl());
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod(REQUEST_METHOD);
        connection.setRequestProperty(CONTENT_TYPE_HEADER, CONTENT_TYPE);
        connection.setRequestProperty(AUTHORIZATION_TOKEN_HEADER, AUTHORIZATION_PREFIX + getToken());
        connection.setRequestProperty(API_KEY_HEADER, apiKey);
        logger.debug("authorization header value: " + AUTHORIZATION_TOKEN_HEADER + getToken());
        return getChargeCodeDetails(connection);
    }

    public String getChargeCodeDetails() throws IOException {
        URL url = new URL(getServiceUrl());
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod(REQUEST_METHOD);
        connection.setRequestProperty(CONTENT_TYPE_HEADER, CONTENT_TYPE);
        connection.setRequestProperty(AUTHORIZATION_HEADER, AUTHORIZATION_PREFIX + getToken());
        logger.debug("authorization header value: " + AUTHORIZATION_PREFIX + getToken());
        return getChargeCodeDetails(connection);
    }

    private String responsePostProcessing(String response) {
        JSONObject root = new JSONObject(response);
        // remove context field
        root.remove(JSON_CONTEXT_FIELD_TO_REMOVE);
        // rename details field key to suitable
        root.put(JSON_FIELD_NEW_NAME, root.getJSONArray(JSON_FIELD_TO_RENAME));
        root.remove(JSON_FIELD_TO_RENAME);
        // add new root field for compatibility with oracle interface implementation
        root.put(JSON_ROOT_FIELD_TO_ADD, new JSONObject());
        root.getJSONObject(JSON_ROOT_FIELD_TO_ADD).put(JSON_FIELD_NEW_NAME, root.getJSONArray(JSON_FIELD_NEW_NAME));
        root.remove(JSON_FIELD_NEW_NAME);
        return root.toString();
    }

    private String getBodyJson(String chargeCodes) {
        JSONObject root = new JSONObject();
        JSONArray chargeCodesJson = new JSONArray();
        for (String chargeCode : chargeCodes.split(",")) {
            chargeCodesJson.put(chargeCode);
        }
        root.put(CHARGE_CODES_JSON_FIELD_NAME, chargeCodesJson);
        root.put(MFMP_INVALID_JSON_FIELD_NAME, Boolean.valueOf(MFMP_INVALID_JSON_FIELD_VALUE));
        root.put(PERS_NUM_JSON_FIELD_NAME, PERS_NUM_JSON_FIELD_VALUE);
        return root.toString(JSON_INDENTATION);
    }

    public String getServiceUrlVar() {
        return serviceUrlVar;
    }

    public void setServiceUrlVar(String serviceUrlVar) {
        this.serviceUrlVar = serviceUrlVar;
    }

    public String getChargeCodesVar() {
        return chargeCodesVar;
    }

    public void setChargeCodesVar(String chargeCodesVar) {
        this.chargeCodesVar = chargeCodesVar;
    }

    public String getResponseVar() {
        return responseVar;
    }

    public void setResponseVar(String responseVar) {
        this.responseVar = responseVar;
    }

    public String getServiceUrl() {
        return serviceUrl;
    }

    public void setServiceUrl(String serviceUrl) {
        this.serviceUrl = serviceUrl;
    }

    public String getChargeCodes() {
        return chargeCodes;
    }

    public void setChargeCodes(String chargeCodes) {
        this.chargeCodes = chargeCodes;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getTokenVar() {
        return tokenVar;
    }

    public void setTokenVar(String tokenVar) {
        this.tokenVar = tokenVar;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getApiKeyVar() {
        return apiKeyVar;
    }

    public void setApiKeyVar(String apiKeyVar) {
        this.apiKeyVar = apiKeyVar;
    }

    public String getTokenTypeVar() {
        return tokenTypeVar;
    }

    public void setTokenTypeVar(String tokenTypeVar) {
        this.tokenTypeVar = tokenTypeVar;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    public String getTokenType() {
        return tokenType;
    }
}
