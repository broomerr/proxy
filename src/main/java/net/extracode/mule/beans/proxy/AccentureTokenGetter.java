package net.extracode.mule.beans.proxy;

import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.mule.api.MuleEventContext;
import org.mule.api.lifecycle.Callable;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.*;
import java.util.HashMap;
import java.util.Map;

public class AccentureTokenGetter implements Callable {

    private static final Logger logger = LogManager.getLogger(AccentureTokenGetter.class);

    private static final String CONTENT_TYPE_HEADER = "Content-Type";
    private static final String CONTENT_TYPE = "application/x-www-form-urlencoded";
    private static final String REQUEST_METHOD = "POST";
    private static final String ENCODING = "UTF-8";
    private static final String TOKEN_JSON_FIELD = "access_token";
    private static final String GRANT_TYPE_PARAM_NAME = "grant_type";
    private static final String CLIENT_TYPE_PARAM_NAME = "client_id";
    private static final String CLIENT_SECRET_PARAM_NAME = "client_secret";
    private static final String USERNAME_PARAM_NAME = "username";
    private static final String PASSWORD_PARAM_NAME = "password";
    private static final String SCOPE_PARAM_NAME = "scope";

    private static final String OAUTH1_TOKEN_TYPE = "oauth1";
    private static final String OAUTH2_TOKEN_TYPE = "oauth2";

    private static final String OAUTH2_GRANT_TYPE = "client_credentials";

    // http request parameters
    private String url;
    private String grantType;
    private String username;
    private String password;
    private String scope;
    private String tokenType;

    private String urlVar;
    private String grantTypeVar;
    private String usernameVar;
    private String passwordVar;
    private String scopeVar;
    private String tokenTypeVar;
    private String lastGottedToken;
    private long lastTokenGettingTimeStamp;
    private String lastGottedTokenVar;

    @Override
    public Object onCall(MuleEventContext muleEventContext) throws Exception {

        url = muleEventContext.getMessage().getInvocationProperty(getUrlVar());
        if (url == null) throw new NullPointerException("service url is not defined");
        grantType = muleEventContext.getMessage().getInvocationProperty(getGrantTypeVar());
        if (grantType == null) throw new NullPointerException("grant type is not defined");
        username = muleEventContext.getMessage().getInvocationProperty(getUsernameVar());
        if (username == null) throw new NullPointerException("username is not defined");
        password = muleEventContext.getMessage().getInvocationProperty(getPasswordVar());
        if (password == null) throw new NullPointerException("password is not defined");
        scope = muleEventContext.getMessage().getInvocationProperty(getScopeVar());
        if (scope == null) throw new NullPointerException("scope is not defined");
        tokenType = muleEventContext.getMessage().getInvocationProperty(getTokenTypeVar());
        if (tokenType == null) throw new NullPointerException("token type is not defined");

        if (checkTokenGettingIsNeeded()) {
            if (tokenType.equals(OAUTH1_TOKEN_TYPE)) {
                logger.info("token type is oauth1");
                lastGottedToken = getToken();
            } else if (tokenType.equals(OAUTH2_TOKEN_TYPE)) {
                logger.info("token type is oauth2");
                lastGottedToken = getOauth2Token();
            }
            lastTokenGettingTimeStamp = System.currentTimeMillis();
        }

        muleEventContext.getMessage().setInvocationProperty(getLastGottedTokenVar(), getLastGottedToken());

        return muleEventContext.getMessage();
    }

    private boolean checkTokenGettingIsNeeded() {
        long currentTimestamp = System.currentTimeMillis();
        return (currentTimestamp - lastTokenGettingTimeStamp > (1000 * 60 * 60));
    }

    public String getToken(HashMap<String, String> parameters) throws IOException {
        URL url = new URL(getUrl());
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod(REQUEST_METHOD);
        connection.setRequestProperty(CONTENT_TYPE_HEADER, CONTENT_TYPE);
        connection.setDoOutput(true);
        connection.setDoInput(true);
        try (DataOutputStream dataOutputStream = new DataOutputStream(connection.getOutputStream())) {
            dataOutputStream.write(getBodyString(parameters).getBytes(ENCODING));
        }
        int responseCode = connection.getResponseCode();
        logger.debug("responseCode: " + responseCode);
        String response = IOUtils.toString(connection.getInputStream(), ENCODING);
        logger.debug("response:\n" + response);
        if (responseCode != 200) {
            logger.error("not successful response code: " + responseCode);
            throw new RuntimeException("not successful response code: " + responseCode);
        }
        JSONObject root = new JSONObject(response);
        return root.getString(TOKEN_JSON_FIELD);
    }

    public String getToken() throws IOException {
        return getToken(getParams());
    }

    public String getOauth2Token() throws IOException {
        return getToken(getOauth2Params());
    }

    private String getBodyString(HashMap<String, String> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for (Map.Entry<String, String> entry : params.entrySet()) {
            if (first)
                first = false;
            else
                result.append("&");
            result.append(URLEncoder.encode(entry.getKey(), ENCODING));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), ENCODING));
        }
        return result.toString();
    }

    private HashMap<String, String> getParams() {
        HashMap<String, String> result = new HashMap<>();
        result.put(GRANT_TYPE_PARAM_NAME, getGrantType());
        result.put(USERNAME_PARAM_NAME, getUsername());
        result.put(PASSWORD_PARAM_NAME, getPassword());
        result.put(SCOPE_PARAM_NAME, getScope());
        return result;
    }

    private HashMap<String, String> getOauth2Params() {
        HashMap<String, String> result = new HashMap<>();
        result.put(GRANT_TYPE_PARAM_NAME, OAUTH2_GRANT_TYPE);
        result.put(CLIENT_TYPE_PARAM_NAME, getUsername());
        result.put(CLIENT_SECRET_PARAM_NAME, getPassword());
        result.put(SCOPE_PARAM_NAME, getScope());
        return result;
    }

    public AccentureTokenGetter() {
    }

    public AccentureTokenGetter(String url, String grantType,
                                String username, String password,
                                String scope, String tokenType) {
        this.url = url;
        this.grantType = grantType;
        this.username = username;
        this.password = password;
        this.scope = scope;
        this.tokenType = tokenType;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getGrantType() {
        return grantType;
    }

    public void setGrantType(String grantType) {
        this.grantType = grantType;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getUrlVar() {
        return urlVar;
    }

    public void setUrlVar(String urlVar) {
        this.urlVar = urlVar;
    }

    public String getGrantTypeVar() {
        return grantTypeVar;
    }

    public void setGrantTypeVar(String grantTypeVar) {
        this.grantTypeVar = grantTypeVar;
    }

    public String getUsernameVar() {
        return usernameVar;
    }

    public void setUsernameVar(String usernameVar) {
        this.usernameVar = usernameVar;
    }

    public String getPasswordVar() {
        return passwordVar;
    }

    public void setPasswordVar(String passwordVar) {
        this.passwordVar = passwordVar;
    }

    public String getScopeVar() {
        return scopeVar;
    }

    public void setScopeVar(String scopeVar) {
        this.scopeVar = scopeVar;
    }

    public String getLastGottedToken() {
        return lastGottedToken;
    }

    public void setLastGottedToken(String lastGottedToken) {
        this.lastGottedToken = lastGottedToken;
    }

    public String getLastGottedTokenVar() {
        return lastGottedTokenVar;
    }

    public void setLastGottedTokenVar(String lastGottedTokenVar) {
        this.lastGottedTokenVar = lastGottedTokenVar;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType() {
        this.tokenType = tokenType;
    }

    public String getTokenTypeVar() {
        return tokenTypeVar;
    }

    public void setTokenTypeVar(String tokenTypeVar) {
        this.tokenTypeVar = tokenTypeVar;
    }
}
