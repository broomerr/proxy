# Лог изменений
Проект придерживается [семантического версионирования](http://semver.org/) и
[принципов ведения changelog](http://keepachangelog.com/)

## [0.3.3] 2018-02-21
### Added:
+ A way to request Oauth2 token

## [0.3.2] 2017-04-04
### Changed:
+ Oracle interface compatibility response post processing

## [0.3.1] 2017-04-03
### Fixed:
+ Accenture service call ready to use in mule app

## [0.3.0] 2017-03-31
### Changed:
+ Accenture service call architecture to use OAuth

## [0.2.1] 2017-02-10
### Fixed:
+ double url encoding for chargeCodes fix

## [0.2.0] 2017-01-13
### Added:
+ accenture service call bean with tests

## [0.1.0] 2017-01-10
### Added:
### Changed:
### Deprecated:
### Removed:
### Fixed:
### Security:
